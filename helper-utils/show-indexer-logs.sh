#!/bin/bash

kubectl -n default logs -f $(kubectl -n default get pods | grep indexer | grep Running | awk '{print $1}')
