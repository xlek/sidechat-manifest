#!/bin/bash

kubectl port-forward svc/platform 8080:18080 &
kubectl port-forward service/jaeger 16686:16686 &
kubectl port-forward service/hwzcrawler 18888:8888 &
kubectl port-forward service/stock-hwzcrawler 8889:8889 &
kubectl port-forward service/elasticsearch 9200:9200 &
kubectl port-forward service/kafka 9092:9092 &
kubectl port-forward service/minio 9000:9000 &
kubectl port-forward service/minio 9090:9090 &
kubectl port-forward service/filestash 8334:8334 &
