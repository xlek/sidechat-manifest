#!/bin/bash

kubectl -n default logs -f $(kubectl -n default get pods | grep media-uploader | grep Running | awk '{print $1}')
