#!/bin/bash
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm repo add stable https://charts.helm.sh/stable/
helm repo update

# admin/prom-operator
helm install rancher-monitoring prometheus-community/kube-prometheus-stack -f prometheus-value.yml
