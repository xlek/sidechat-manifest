rs.initiate(
   {
      _id: "myReplSet",
      version: 1,
      members: [
         { _id: 0, host : "mongodb-0:27017" },
         { _id: 1, host : "mongodb-1:27017" },
         { _id: 2, host : "mongodb-2:27017" }
      ]
   }
)