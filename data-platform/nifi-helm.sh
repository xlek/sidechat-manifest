#!/bin/bash

helm repo add cetic https://cetic.github.io/helm-charts
helm install -n default --set persistence.enabled=True --set service.type=NodePort --set properties.sensitiveKey=password --set auth.singleUser.username=admin --set auth.singleUser.password=Password123456 nifi cetic/nifi
