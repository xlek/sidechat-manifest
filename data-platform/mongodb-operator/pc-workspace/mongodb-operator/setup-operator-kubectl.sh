#!/bin/bash

source ~/.bashrc
kubectl apply -f ./mongodb-enterprise-kubernetes/crds.yaml
kubectl apply -f ./mongodb-enterprise-kubernetes/mongodb-enterprise.yaml
