helm repo add mongodb https://mongodb.github.io/helm-charts
helm install enterprise-operator mongodb/enterprise-operator
helm install enterprise-operator mongodb/enterprise-operator \
  --namespace mdb \
  --create-namespace

kubectl describe deployments mongodb-enterprise-operator -n mdb
